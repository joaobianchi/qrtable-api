const express = require('express')
const router = express.Router()

const authController = require('../controllers/authController')
const userController = require('../controllers/userController')

router.post('/authentication/login', authController.login)
router.post('/authentication/logout', authController.logout)

router.post('/user', userController.registerUser)
router.put('/user/:id', userController.updateUser)
router.delete('/user/:id', userController.destroyUser)
router.get('/user/:id', userController.infoUser)
router.get('/user', userController.indexUser)

module.exports = router