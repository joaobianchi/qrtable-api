'use strict'

const AuthService = require('../services/authService')
const authService = new AuthService()

function login(req, res) {
  const { body } = req

  !body.email || !body.password || body.password.length < 8
    ? res.status(400).send({ error: 'Dados informados incompletos, favor verificar documentação' })
    : authService.login(body)
}

function logout(req, res) {
  const { headers } = req
  console.log(req, 'HEADEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEER')

  // !headers.token
  //   ? res.status(400).send({ error: 'Dados informados incompletos, favor verificar documentação' })
  //   : authService.logout(body)
}

module.exports = { login, logout }