'use strict'

const UserService = require('../services/userService')
const userService = new UserService()

function registerUser(req, res) {
  const { body } = req

  if (!body.name || !body.email || !body.password || !body.uf || !body.phone
    || !body.city || body.password.length < 8 || body.uf.length > 2
    || body.phone.length > 12) {

    return res.status(400).send({ error: 'Dados informados incompletos ou incorretos, favor verificar documentação' })
  }

  return userService.registerUser(body).then(result => {
    if (result) {
      return res.status(200).send({ message: 'Usuário inserido com sucesso!', data: result })
    }
  })
    .catch(error => {
      return res.status(400).send({ message: error.message })
    })
}

function updateUser(req, res) {
  const { body, params } = req

  !params.id || !body.name || !body.email || !body.password || !body.uf || !body.city
    || body.password.length < 8 || body.uf.length > 2
    ? res.status(400).send({ error: 'Dados informados incompletos ou incorretos, favor verificar documentação' })
    : userService.updateUser(body)
}

function destroyUser(req, res) {
  const { params } = req

  !params.id
    ? res.status(400).send({ error: 'Dados informados incompletos ou incorretos, favor verificar documentação' })
    : userService.destroyUser(body)
}

function infoUser(req, res) {
  const { body } = req

  return userService.indexUser(body)
}

function indexUser(req, res) {
  const { body } = req

  return userService.indexUser(body)
}


module.exports = { registerUser, updateUser, infoUser, destroyUser, indexUser }