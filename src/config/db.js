require('dotenv').config()
const mongoose = require('mongoose')

const uriConn = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PSWD}@cluster0.ojobt.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`

mongoose.connect(
  uriConn,
  { useNewUrlParser: true, useUnifiedTopology: true }
)
mongoose.Promise = global.Promise

const connection = mongoose.connection
module.exports = connection