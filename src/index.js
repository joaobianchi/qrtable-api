const express = require('express')
const bodyParser = require('body-parser')
const router = require('./routes')
require('dotenv').config()

const app = express()
const cors = require('cors');

const connection = require('../src/config/db')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/api', router)

app.get('/', (req, res) => res.send({ message: 'Server is Running!' }))

app.listen(5000, () => console.log(`Server is running on host: ${process.env.API_HOST}:${process.env.API_PORT}`))
connection.once('open', () => console.log('MongoDB database connection established successfully.'))

module.exports = app