const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
    maxlength: 12,
    minlength: 11
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  uf: {
    type: String,
    required: true,
    maxlength: 2,
    minlength: 2,
  },
  city: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  }
})

const User = mongoose.model('User', UserSchema)
module.exports = User