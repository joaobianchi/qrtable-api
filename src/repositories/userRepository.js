'use strict'

const { User } = require('../models')

class UserRepository {

  constructor() { }

  async registerUser(options) {
    return await User.create(options)
      .then(result => result)
      .catch(error => {

        if (error.keyValue.email === options.email)
          throw new Error('E-mail já possui cadastro em nosso banco de dados.')

        if (error.keyValue.phone === options.phone)
          throw new Error('Número de telefone já possui cadastro em nosso banco de dados.')
      })
  }

  updateUser(options) {
    // try {
    //       const user = await User.create(options)
    //       return res.send({ user })
    //     } catch (error) {
    //       return res.status(400).send({ error: 'Registration failed ' })
    //     }
  }

  destroyUser(options) {
    // try {
    //       const user = await User.create(options)
    //       return res.send({ user })
    //     } catch (error) {
    //       return res.status(400).send({ error: 'Registration failed ' })
    //     }
  }

  infoUser(options) {
    // try {
    //       const user = await User.create(options)
    //       return res.send({ user })
    //     } catch (error) {
    //       return res.status(400).send({ error: 'Registration failed ' })
    //     }
  }

  indexUser(options) {
    // try {
    //       const user = await User.create(options)
    //       return res.send({ user })
    //     } catch (error) {
    //       return res.status(400).send({ error: 'Registration failed ' })
    //     }
  }
}

module.exports = UserRepository