'use strict'

const UserRepository = require('../repositories/userRepository')
const userRepository = new UserRepository()

class UserService {

  constructor() { }

  registerUser(options) {
    return userRepository.registerUser(options)
      .then(result => result)
      .catch(error => {
        throw error
      })
  }

  infoUser(options) {
    return userRepository.infoUser(options)
      .then(result => result)
      .catch(error => error)
  }

  updateUser(options) {
    return userRepository.updateUser(options)
      .then(result => result)
      .catch(error => error)
  }

  destroyUser(options) {
    return userRepository.destroyUser(options)
      .then(result => result)
      .catch(error => error)
  }

  indexUser(options) {
    return userRepository.indexUser(options)
      .then(result => result)
      .catch(error => error)
  }
}

module.exports = UserService