'use strict'

const AuthRepository = require('../repositories/authRepository')
const authRepository = new AuthRepository()

class AuthService {

  constructor() { }

  login(body) {
    return authRepository.login(body)
  }

  logout(body) {
    return authRepository.logout(body)
  }
}

module.exports = AuthService