const { request } = require('../src/index')
const app = require('../src/index')

test('Testing root path', async () => {
  return await app.get('/', (req, res) => expect(res.status).toBe(200))
})